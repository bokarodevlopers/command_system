@extends('layouts.admin')
@section('content')
<h2 style="margin-top: 12px;" class="alert alert-success text-center">Command's Table</h2>
<div class="col-sm-9" style="margin-top:15px;">
   <div class="table table-responsive">
      <table class="table table-bordered" id="table">
         <tr>
            <th>No</th>
            <th width="150px">Tools</th>
            <th>Commands</th>
            <th class="text-center" width="150px">
               <a href="#" class="create-modal btn btn-success btn-sm">
                  <i class="glyphicon glyphicon-plus"></i>
               </a>
            </th>
         </tr>
         {{ csrf_field() }}
         <?php  $no=1; ?>
         @foreach ($commands as $value)
         <tr class="post{{$value->id}}">
            <td>{{ $no++ }}</td>
            <td>{{ $value->tool_slug }}</td>
            <td>{{ $value->command }}</td>
            <td>
               <a href="#" class="show-modal btn btn-info btn-sm" data-id="{{$value->id}}"
                  data-title="{{$value->command}}" data-body="{{$value->about}}">
                  <i class="glyphicon glyphicon-zoom-in"></i>
               </a>
               <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{$value->id}}"
                  data-title="{{$value->command}}" data-body="{{$value->about}}">
                  <i class="glyphicon glyphicon-pencil"></i>
               </a>
               <a href="#" class="delete-modal btn btn-danger btn-sm" data-id="{{$value->id}}" data-cmd_slug="{{$value->cmd_slug}}"
                  data-title="{{$value->command}}" data-body="{{$value->about}}">
                  <i class="glyphicon glyphicon-trash"></i>
               </a>
            </td>
         </tr>
         @endforeach
      </table>
   </div>
   {{$commands->links()}}
</div>
<!-- {{-- Modal Form Create Post --}} -->
<div id="create" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <form class="form-horizontal" role="form">
               <div class="form-group row add">
                  <label class="control-label col-sm-2" for="title">Select Tool:</label>
                  <div class="col-sm-10">
                     <select class="form-control form-control-md" name="tool_slug" id="tool_slug">
                        @foreach($tools as $tool)
                        <option value="{{ $tool->tool }}">{{ $tool->tool}}</option>
                        @endforeach
                     </select>
                     <p class="error text-center alert alert-danger hidden"></p>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-sm-2" for="body">Command :</label>
                  <div class="col-sm-10">
                     <textarea type="text" class="form-control" id="command" name="command"
                        placeholder="Write a Command" required></textarea>
                     <p class="error text-center alert alert-danger hidden"></p>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button class="btn btn-primary" type="submit" id="add">
               <span class="glyphicon glyphicon-plus"></span>Save Command
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
               <span class="glyphicon glyphicon-remobe"></span>Close
            </button>
         </div>
      </div>
   </div>
</div>
</div>
<!-- {{-- Modal Form Show POST --}} -->
<div id="show" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <!-- <div class="form-group">
               <label for="">ID :</label>
               <b id="i" />
            </div> -->
            <div class="form-group">
               <label for="">Command :</label>
               <b id="ti" />
            </div>
            <!-- <div class="form-group">
                      <label for="">Command :</label>
                      <b id="by"/>
                    </div> -->
         </div>
      </div>
   </div>
</div>
<!-- {{-- Modal Form Edit and Delete Post --}} -->
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <form class="form-horizontal" role="modal">
               <div class="form-group">
                  <label class="control-label col-sm-2" for="id">ID</label>
                  <div class="col-sm-10">
                     <input type="hidden" class="form-control" id="fid" disabled>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-sm-2" for="title">Command</label>
                  <div class="col-sm-10">
                     <textarea type="name" class="form-control" id="t"></textarea>
                  </div>
               </div>
               <!-- <div class="form-group">
                            <label class="control-label col-sm-2"for="body">Command</label>
                            <div class="col-sm-10">
                            <textarea type="name" class="form-control" id="b"></textarea>
                            </div>
                          </div> -->
            </form>
            <!-- {{-- Form Delete Post --}} -->
            <div class="deleteContent">
               Are You sure want to delete <span class="title"></span>?
               <span class="hidden id"></span>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn actionBtn" data-dismiss="modal">
               <span id="footer_action_button" class="glyphicon"></span>
            </button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">
               <span class="glyphicon glyphicon"></span>close
            </button>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
// {{-- ajax Form Add Post--}}
$(document).on('click', '.create-modal', function() {
   $('#create').modal('show');
   $('.form-horizontal').show();
   $('.modal-title').text('Add Command');
});
$("#add").click(function() {
   $.ajax({
      type: 'POST',
      url: 'addCommand',
      data: {
         '_token': $('input[name=_token]').val(),
         'tool_slug': $('select[name=tool_slug]').val(),
         'command': $('textarea[name=command]').val()
      },
   });
   $('#tool_slug').val('');
   $('#command').val('');
   $('#create').modal('hide');
   location.reload();
});

// function Edit POST
$(document).on('click', '.edit-modal', function() {
   $('#footer_action_button').text(" Update Post");
   $('#footer_action_button').addClass('glyphicon-check');
   $('#footer_action_button').removeClass('glyphicon-trash');
   $('.actionBtn').addClass('btn-success');
   $('.actionBtn').removeClass('btn-danger');
   $('.actionBtn').addClass('edit');
   $('.modal-title').text('Command Edit');
   $('.deleteContent').hide();
   $('.form-horizontal').show();
   $('#fid').val($(this).data('id'));
   $('#t').val($(this).data('title'));
   // $('#b').val($(this).data('command'));
   $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.edit', function() {
   $.ajax({
      type: 'POST',
      url: 'editCommand',
      data: {
         '_token': $('input[name=_token]').val(),
         'id': $("#fid").val(),
         'command': $('#t').val()
         // 'body': $('#b').val()
      },
   });
   location.reload();
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
   $('#footer_action_button').text(" Delete");
   $('#footer_action_button').removeClass('glyphicon-check');
   $('#footer_action_button').addClass('glyphicon-trash');
   $('.actionBtn').removeClass('btn-success');
   $('.actionBtn').addClass('btn-danger');
   $('.actionBtn').addClass('delete');
   $('.modal-title').text('Delete Tool');
   $('.id').text($(this).data('cmd_slug'));
   $('.deleteContent').show();
   $('.form-horizontal').hide();
   $('.title').html($(this).data('title'));
   $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function() {
   $.ajax({
      type: 'POST',
      url: 'deleteCommand',
      data: {
         '_token': $('input[name=_token]').val(),
         'cmd_slug': $('.id').text()
      },
      success: function(data) {
         alert(data['message']); 
         $('.post' + $('.command').text()).remove();
      }
   });
   location.reload();
});
// Show function
$(document).on('click', '.show-modal', function() {
   $('#show').modal('show');
   $('#i').text($(this).data('id'));
   $('#ti').text($(this).data('title'));
   $('#by').text($(this).data('command'));
   $('.modal-title').text('Show Command');
});
</script>
@endsection