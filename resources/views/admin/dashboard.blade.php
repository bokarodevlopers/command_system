@extends('layouts.admin')
@section('content')

<div class="col-sm-9">
<div class="jumbotron jumbotron-billboard">
      <div class="img"></div>
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <h2 class="text-primary">Dashboard</h2>
            </div>
         </div>
      </div>
   </div>
  <table class="table table-borderless">
   <thead>
      <tr>
         <th>Tool</th>
         <th>Command</th>
         <th>Example</th>
      </tr>
   </thead>
   <tbody>
      <tr>
      <td><h3><span class="label label-danger">{{$tool}}</span> <span class="label label-primary">Tools</span></h3></td>
      <td><h3><span class="label label-info">{{$command}}</span> <span class="label label-primary">Commands</span></h3></td>
      <td><h3><span class="label label-success">{{$example}}</span> <span class="label label-primary">Examples</span></h3></td>
      </tr>
   </tbody>
  </table>
</div>
   @endsection