@extends('layouts.admin')
@section('content')

<h2 style="margin-top: 12px;" class="alert alert-success text-center">Example's Table</h2>
<div class="col-sm-9" style="margin-top:15px;">
   <div class="table table-responsive">
      <table  class="table table-bordered" id="table">
         <tr>
            <th width="15px">No</th>
            <th>Examples</th>
            <th class="text-center" width="150px">
               <a href="#" class="create-modal btn btn-success btn-sm">
                  <i class="glyphicon glyphicon-plus"></i>
               </a>
            </th>
         </tr>
         {{ csrf_field() }}
         <?php  $no=1; ?>
         @foreach ($examples as $value)
         <tr class="post{{$value->id}}">
            <td>{{ $no++ }}</td>
            <td>{{ $value->example }}</td>
            <td>
               <a href="#" class="show-modal btn btn-info btn-sm" data-id="{{$value->id}}"
                  data-body="{{$value->example}}">
                  <i class="glyphicon glyphicon-zoom-in"></i>
               </a>
               <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{$value->id}}"
                  data-body="{{$value->example}}">
                  <i class="glyphicon glyphicon-pencil"></i>
               </a>
               <a href="#" class="delete-modal btn btn-danger btn-sm" data-id="{{$value->id}}"
                  data-body="{{$value->example}}">
                  <i class="glyphicon glyphicon-trash"></i>
               </a>
            </td>
         </tr>
         @endforeach
      </table>
   </div>
</div>
<!-- {{-- Modal Form Create Post --}} -->
<div id="create" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <form class="form-horizontal" role="form">
               <div class="form-group row add">
                  <label class="control-label col-sm-2" for="title">Select Tool:</label>
                  <div class="col-sm-10">
                     <select class="form-control form-control-md" name="tool_slug" id="tool_slug">
                        <option>--Select--</option>
                        @foreach($tools as $tool)
                        <option value="{{ $tool->slug }}">{{ $tool->tool}}</option>
                        @endforeach
                     </select>
                     <p class="error text-center alert alert-danger hidden"></p>
                  </div>
               </div>
               <div class="form-group row add">
                  <label class="control-label col-sm-2" for="command">Command:</label>
                  <div class="col-sm-10">
                     <select class="form-control form-control-md" name="cmd_slug" id="cmd_slug">
                     </select>
                     <p class="error text-center alert alert-danger hidden"></p>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-sm-2" for="body">Example :</label>
                  <div class="col-sm-10">
                     <textarea type="text" class="form-control" id="example" name="example" rows="10"
                        placeholder="Write An Example" required></textarea>
                     <p class="error text-center alert alert-danger hidden"></p>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button class="btn btn-primary" type="submit" id="add">
               <span class="glyphicon glyphicon-plus"></span>Save Post
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
               <span class="glyphicon glyphicon-remobe"></span>Close
            </button>
         </div>
      </div>
   </div>
</div>
</div>
<!-- {{-- Modal Form Show POST --}} -->
<div id="show" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <!-- <div class="form-group">
               <label for="">ID :</label>
               <b id="i" />
            </div> -->
            <div class="form-group">
               <label for="">Example :</label>
               <b id="by" />
            </div>
            <!-- <div class="form-group">
                      <label for="">Command :</label>
                      <b id="by"/>
                    </div> -->
         </div>
      </div>
   </div>
</div>
<!-- {{-- Modal Form Edit and Delete Post --}} -->
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <form class="form-horizontal" role="modal">
               <div class="form-group">
                  <label class="control-label col-sm-2" for="id"></label>
                  <div class="col-sm-10">
                     <input type="hidden" class="form-control" id="fid" disabled>
                  </div>
               </div>
               <!-- <div class="form-group">
                            <label class="control-label col-sm-2"for="cmd_slug">Example</label>
                            <div class="col-sm-10">
                            <textarea type="name" class="form-control" id="t"></textarea>
                            </div>
                          </div> -->
               <div class="form-group">
                  <label class="control-label col-sm-2" for="example">Example</label>
                  <div class="col-sm-10">
                     <textarea type="name" class="form-control" id="b"></textarea>
                  </div>
               </div>
            </form>
            {{-- Form Delete Post --}}
            <div class="deleteContent">
               Are You sure want to delete <span class="title"></span>?
               <span class="hidden id"></span>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn actionBtn" data-dismiss="modal">
               <span id="footer_action_button" class="glyphicon"></span>
            </button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">
               <span class="glyphicon glyphicon"></span>close
            </button>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
// {{-- ajax Form Add Post--}}
$(document).on('click', '.create-modal', function() {
   $('#create').modal('show');
   $('.form-horizontal').show();
   $('.modal-title').text('Add Example');
});
$("#add").click(function() {
   $.ajax({
      type: 'POST',
      url: 'addExample',
      data: {
         '_token': $('input[name=_token]').val(),
         'cmd_slug': $('select[name=cmd_slug]').val(),
         'example': $('textarea[name=example]').val()
      },
   });
   $('#cmd_slug').val('');
   $('#body').val('');
   $('#create').modal('hide');
   alert("Tool SuccessFully Added");
   location.reload();
});
// function Edit POST
$(document).on('click', '.edit-modal', function() {
   $('#footer_action_button').text(" Update Example");
   $('#footer_action_button').addClass('glyphicon-check');
   $('#footer_action_button').removeClass('glyphicon-trash');
   $('.actionBtn').addClass('btn-success');
   $('.actionBtn').removeClass('btn-danger');
   $('.actionBtn').addClass('edit');
   $('.modal-title').text('Example Edit');
   $('.deleteContent').hide();
   $('.form-horizontal').show();
   $('#fid').val($(this).data('id'));
   $('#t').val($(this).data('title'));
   $('#b').val($(this).data('body'));
   $('#myModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function() {
   $.ajax({
      type: 'POST',
      url: 'editExample',
      data: {
         '_token': $('input[name=_token]').val(),
         'id': $("#fid").val(),
         'title': $('#t').val(),
         'body': $('#b').val()
      },
   });
   location.reload();
});

// form Delete function
$(document).on('click', '.delete-modal', function() {
   $('#footer_action_button').text(" Delete");
   $('#footer_action_button').removeClass('glyphicon-check');
   $('#footer_action_button').addClass('glyphicon-trash');
   $('.actionBtn').removeClass('btn-success');
   $('.actionBtn').addClass('btn-danger');
   $('.actionBtn').addClass('delete');
   $('.modal-title').text('Delete Examplple');
   $('.id').text($(this).data('id'));
   $('.deleteContent').show();
   $('.form-horizontal').hide();
   $('.body').html($(this).data('body'));
   $('#myModal').modal('show');
});

$('.modal-footer').on('click', '.delete', function() {
   $.ajax({
      type: 'POST',
      url: 'deleteExample',
      data: {
         '_token': $('input[name=_token]').val(),
         'id': $('.id').text()
      },
      success: function(data) {
         $('.post' + $('.example').text()).remove();
      }

   });
   location.reload();
});
// Show function
$(document).on('click', '.show-modal', function() {
   $('#show').modal('show');
   $('#i').text($(this).data('id'));
   $('#ti').text($(this).data('title'));
   $('#by').text($(this).data('body'));
   $('.modal-title').text('Show Command');
});

// Drop Down -------------------
$('#tool_slug').change(function() {
   var tool_slug = $(this).val();
   if (tool_slug) {
      $.ajax({
         type: "GET",
         url: "{{url('command_list')}}/" + tool_slug,
         success: function(res) {
            if (res) {
               $("#cmd_slug").empty();
               $("#cmd_slug").append('<option>Select</option>');
               $.each(res, function(key, value) {
                  $("#cmd_slug").append('<option value="' + key + '">' + value + '</option>');
               });

            } else {
               $("#cmd_slug").empty();
            }
         }
      });
   } else {
      $("#cmd_slug").empty();
   }
});
</script>
@endsection