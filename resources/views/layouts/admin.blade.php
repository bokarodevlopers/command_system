<!DOCTYPE html>
<html lang="en">

<head>
   <title>Admin</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   <style>
   /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
   .row.content {
      height: 1500px
   }

   /* Set gray background color and 100% height */
   .sidenav {
      background-color: #f1f1f1;
      height: 100%;
   }

   /* Set black background color, white text and some padding */
  
   /* On small screens, set height to 'auto' for sidenav and grid */
   @media screen and (max-width: 767px) {
      .sidenav {
         height: auto;
         padding: 15px;
      }

      .row.content {
         height: auto;
      }
   }
   h4{
     color:#DF1D10;
   }
   a {
  font-weight: bold;
  font-size: 18px;
}
   </style>
</head>

<body>
   <div class="container-fluid">
      <div class="row content">
         <div class="col-sm-3 sidenav">
         <img src="{{URL::asset('/img/logo.png')}}" alt="profile Pic" height="40">
            <img src="https://i.pinimg.com/originals/0f/8b/28/0f8b2870896edcde8f6149fe2733faaf.jpg" alt="profile Pic" height="80">
            <ul class="nav nav-pills nav-stacked">
               <li><a class="text-info" href="/home">Home</a></li>
               <li><a class="text-info" href="{{route('tools')}}">Tool</a></li>
               <li><a class="text-info" href="{{route('commands')}}">Command</a></li>
               <li><a class="text-info" href="{{route('examples')}}">Example</a></li>
               <hr>
               <hr>
               <h4>User</h4>
               @guest
               <li class="nav-item">
                  <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
               </li>
               @if (Route::has('register'))
               <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
               </li>
               @endif
               @else
               <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle text-success" href="#" role="button" data-toggle="dropdown"
                     aria-haspopup="true" aria-expanded="false" v-pre>
                     {{ Auth::user()->name }} <span class="caret"></span>
                  </a>


                  <a class="dropdown-item text-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                     {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     @csrf
                  </form>

               </li>
               @endguest
            </ul>
            <br>
            <div class="input-group">

               <span class="input-group-btn">

               </span>
            </div>
         </div>



         <main class="py-4">
            @yield('content')
         </main>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
</body>
</html>