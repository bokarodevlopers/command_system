<!DOCTYPE html>
<html lang="en">

<head>
   <title>Command</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
   <link href="{{ asset('css/footer.css') }}" rel="stylesheet">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
   <style>
   .fakeimg {
      height: 200px;
      background: #aaa;
   }

   p {
      font-family: Gill Sans Extrabold, sans-serif;
   }

   code {
      font-family: Gill Sans Extrabold, sans-serif;
   }
   </style>

</head>

<body>
   <nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
      <img src="{{URL::asset('/img/logo.png')}}" alt="profile Pic" height="40">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
         <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
         <ul class="navbar-nav float-right">
            <li class="nav-item active">
               <a href="#" class="nav-link">Home</a>
            </li>
            <li class="nav-item dropdown">
               <a href="https://www.devopsschool.com/tutorial/" class="nav-link dropdown-toggle"
                  data-toggle="dropdown">Tutorials</a>
               <div class="dropdown-menu">
                  <a href="https://www.devopsschool.com/tutorial/" class="dropdown-item">Web Tutorials</a>
                  <a href="https://www.devopsschool.com/video/" class="dropdown-item">Video Tutorials</a>
                  <a href="https://www.devopsschool.com/slides/" class="dropdown-item">HTML Slides</a>
                  <a href="https://www.devopsschool.com/pdf/" class="dropdown-item">PDF Slides</a>
                  <a href="https://www.devopsschool.com/blog/" class="dropdown-item">Blog</a>
               </div>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsschool.com/courses/" class="nav-link">Courses</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsschool.com/events/" class="nav-link">Events</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsschool.com/clients/" class="nav-link">Clients</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsschool.com/commands/" class="nav-link">Commands</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopscertification.co/" class="nav-link">Certifications</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsschool.com/quiz/" class="nav-link">Quiz</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsconsulting.in/" class="nav-link">Consulting</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsschool.com/testimonials/" class="nav-link">Testimonials</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsschool.com/jobs/" class="nav-link">Jobs</a>
            </li>
            <li class="nav-item">
               <a href="https://www.devopsschool.com/contact/" class="nav-link">Contact</a>
            </li>

         </ul>
      </div>
      <div class="flex-center position-ref full-height">
         @if (Route::has('login'))
         <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}"><i class="fa fa-home fa-2x" aria-hidden="true"></i></a>
            @else
            <a id="btnShowPopup" href="#"><i class="fa fa-user-circle fa-2x" aria-hidden="true"></i></a>


            @if (Route::has('register'))
            <!-- <a href="{{ route('register') }}">Register</a> -->
            @endif
            @endauth
         </div>
         @endif
      </div>
   </nav>

   <div class="container" style="margin-top:30px">
      <p>These are some of the things I could recollect that I find useful on a day to day basis. The list is not
         exhaustive and I believe most of them are applicable in a unix/linux environment.</p>
      <div class="row">
         <div class="col-sm-4">
            <br>
            <br>
            <img src="{{URL::asset('/img/tool.png')}}" alt="">
            <br>
            <h4 style="color:red;">TOOLS</h4>
            @foreach($tools as $tool)
            <ul class="list-group">
               <li class="list-group-item text-lg">
                  <a id="{{ $tool->slug}}" data-toggle="tooltip" title="{{$tool->about}}" href="{{ route('tool', $tool->slug) }}
                     ">{{$tool->tool}}</a>
               </li>
            </ul>
            @endforeach
            <hr class="d-sm-none">
            <div class="card-body">
               <p>DevOps is not a tool or an operating system and as such does not have any particular commands that can
                  be run
                  in case of any issues.</p>
            </div>
         </div>
         <div class="col-sm-8">
            <!-- <h2>TITLE HEADING</h2> -->
            <img src="{{URL::asset('/img/command.png')}}" alt="">
            <input id="myInput" class="form-control" type="text" placeholder="Search Commands" aria-label="Search">
            <br>
            <div class="overflow-auto">
               <table id="myTable" class="table table-striped" data-sort-order="asc">
                  <thead>
                     <tr>
                        <th>LETEST COMMANDS</th>
                        <th width="130px"">EXAMPLE</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($commands as $command)
                  <tr>
                  <td id=" command"><code class="text">{{$command->command}}</code></td>
                        <td>
                           <a class="btn btn-success btn-sm float-right" href="{{url('tools/'.$command->tool_slug .'/'.$command->cmd_slug)}}"
                              role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </td>
                     </tr>
                     @endforeach
                     </tbody>
               </table>
            </div>
            {{$commands->links()}}
         </div>
      </div>
   </div>
   <!-- footer -->
   <footer class="footer-section mt-2">
      <div class="container">
         <div class="footer-cta pb-3 to">
            <div class="row">
               <div class="col-xl-4 col-md-4 mb-30">
                  <div class="single-cta">
                     <i class="fa fa-map-marker"></i>
                     <div class="cta-text">
                        <h4>Find us</h4>
                        <span>www.DevOpsSchool.com</span>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-md-4 mb-30">
                  <div class="single-cta">
                     <i class="fa fa-phone"></i>
                     <div class="cta-text">
                        <h4>Call us</h4>
                        <span> +91 700 483 5706 | +91 700 483 5930 </span>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-md-4 mb-30">
                  <div class="single-cta">
                     <i class="fa fa-envelope-open"></i>
                     <div class="cta-text">
                        <h4>Mail us</h4>
                        <span>contact@devopsschool.com</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer-content pt-2 pb-2">
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-4 mb-50">
                  <div class="footer-widget">
                     <div class="footer-logo">
                        <a href="#"><img src="{{URL::asset('/img/logo.png')}}" alt="profile Pic" class="img-fluid"></a>
                     </div>
                     <div class="footer-text">
                        <h4 class="t3">Bangalore :-</h4>
                        <p>DevOpsSchool Training Venue (RPS Consulting)92, HJS Chambers, Richmond Road,<br>Bangalore -
                           560025</p>
                     </div>
                     <div class="footer-text">
                        <h4 class="t3">Hyderabad :-</h4>
                        <p>DevOpsSchol Training Venue(Palmeto Solutions)<br>8th floor, Vaishnavi Cynosure,Telecom Nagar,
                           Gachibowli, Telangana -500032 <br>Land Mark : Reliance Digital Building, Next to Gachibowli
                           Flyover.</p>
                     </div>

                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 mb-30">
                  <div class="footer-widget">
                     <div class="footer-widget-heading">
                        <h3>Useful Links</h3>
                     </div>
                     <ul>
                        <li><a target="_blank" href="https://www.devopsschool.com/">About Us</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/calender/india.html">Training
                              Calender</a></li>
                        <li><a target="_blank" href="https://www.devopscertification.co/">Certification</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/clients/">Clients</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/comparison/">Comparison</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/gallery/">Gallery</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/testimonials/">Testimonials</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/news/index.html">Newsletter</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/support/">Support</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/trainer/">Trainer</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/trainer/">Trainers</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/training/">Training</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/cv/">CV</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 mb-50">
                  <div class="footer-widget">
                     <div class="footer-widget-heading">
                        <h3>Have Queries? Ask us</h3>
                     </div>
                     <div class="footer-text mb-25">
                        <p>Request more information</p>
                     </div>
                     <div class="subscribe-form">
                        <form action="#">

                           <input type="text" placeholder="Email Address">
                           <button><i class="fa fa-telegram"></i></button>
                        </form>
                     </div>
                  </div>
                  <div class="footer-social-icon mt-4">
                     <span>Follow us</span>
                     <a target="_blank" href="https://www.youtube.com/channel/UCrXr49kBvXJeQMMl2693c4g/featured"><i
                           class="fa fa-youtube facebook-bg"></i></a>
                     <a target="_blank" href="https://www.facebook.com/DevOpsSchool"><i
                           class="fa fa-facebook-f twitter-bg"></i></a>
                     <a target="_blank" href="https://twitter.com/DevOpsSchools"><i
                           class="fa fa-twitter google-bg"></i></a>
                  </div>
                  <div class="footer-social-icon social2">
                     <span></span>
                     <a target="_blank" href="https://www.instagram.com/devopsschool/"><i
                           class="fa fa-instagram facebook-bg"></i></a>
                     <a target="_blank" href="https://www.linkedin.com/company/devopsschool"><i
                           class="fa fa-linkedin twitter-bg"></i></a>
                     <a target="_blank" href="https://www.devopsschool.com/community/"><img
                           src="{{URL::asset('/img/google.png')}}" alt="profile Pic" class="img7"></a>
                  </div>
               </div>

            </div>

         </div>
      </div>
      <div class="copyright-area">
         <div class="container">
            <div class="row">
               <div class="col-xl-6 col-lg-6 text-center text-lg-left">
                  <div class="copyright-text">
                     <p>Copyright &copy; 2019 DevOpsSchool Inc All Rights Reserved<a target="_blank" href="#"></a></p>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 d-none d-lg-block text-right">
                  <div class="footer-menu">
                     <ul>
                        <li><a target="_blank" href="https://www.devopsschool.com/">Home</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/about/terms-and-conditions/">Terms</a>
                        </li>
                        <li><a target="_blank" href="https://www.devopsschool.com/about/privacy-policy/">Privacy
                              Policy</a></li>
                        <li><a target="_blank" href="https://www.devopsschool.com/contact/">Contact</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </footer>
   <div id="MyPopup" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               </h4>
            </div>
            <hr>
            <form method="POST" action="{{ route('login') }}">
               @csrf

               <div class="form-group row">
                  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                  <div class="col-md-6">
                     <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                     @error('email')
                     <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>

               <div class="form-group row">
                  <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                  <div class="col-md-6">
                     <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" required autocomplete="current-password">

                     @error('password')
                     <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>

               <div class="form-group row">
                  <div class="col-md-6 offset-md-4">
                     <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember"
                           {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                           {{ __('Remember Me') }}
                        </label>
                     </div>
                  </div>
               </div>

               <div class="form-group row mb-0">
                  <div class="col-md-8 offset-md-4">
                     <button type="submit" class="btn btn-primary">
                        {{ __('Login') }}
                     </button>

                     @if (Route::has('password.request'))
                     <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                     </a>
                     @endif
                  </div>
               </div>
            </form>
            <br>
            <div class="modal-footer">
               <input type="button" id="btnClosePopup" value="Close" class="btn btn-danger" />
            </div>
         </div>
      </div>
   </div>
</body>
<script>
$(function() {
   $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function() {
   $("#myInput").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#myTable tr").filter(function() {
         $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
   });
});

$(function() {
   $("#btnShowPopup").click(function() {
      $("#MyPopup").modal("show");
   });

   $("#btnClosePopup").click(function() {
      $("#MyPopup").modal("hide");
   });
});
</script>

</html>