<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {

//     return view('welcome');
// });

Route::get('/', 'UserViewController@tool');
Route::get('/tool/{slug}', 'UserViewController@command')->name('tool');
Route::get('tools/{tool_slug}/{cmd_slug}', 'UserViewController@example')->name('command');





Auth::routes();

        Route::get('/home', 'HomeController@index')->name('dashboard');
        // ======================Tool==================
        Route::get('/tools','ToolController@index')->name('tools');
        Route::resource('post','ToolController');
        Route::POST('addPost','ToolController@addPost');
        Route::POST('editPost','ToolController@editPost');
        Route::POST('deletePost','ToolController@deletePost');
        // =======================Command=====================
        Route::get('/commands', 'CommandController@index')->name('commands');
        Route::POST('addCommand','CommandController@store');
        Route::POST('editCommand','CommandController@edit');
        Route::POST('deleteCommand','CommandController@destroy');

        // ========================Example=====================
        Route::get('/examples','ExampleController@index')->name('examples');
        Route::POST('/addExample','ExampleController@addExample');
        Route::get('command_list/{slug}','ExampleController@command_list');
        Route::POST('editExample','ExampleController@editExample');
        Route::POST('deleteExample','ExampleController@destroy');

        


    

   
   
    