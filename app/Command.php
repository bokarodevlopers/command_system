<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    protected $table='commands';
    protected $fillable=['command','tool_slug'];

    public function Tool()
    {
        return $this->belongsTo('App\Command');
    }
}
