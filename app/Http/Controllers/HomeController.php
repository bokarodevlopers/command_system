<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tool;
use App\Command;
use App\Example;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        

        $tool = Tool::all()->count();
        $command = Command::all()->count();
        $example = Example::all()->count();
        return view('admin.dashboard', compact('tool','command','example'));

    }

   
}
