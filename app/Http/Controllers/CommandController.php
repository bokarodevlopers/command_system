<?php

namespace App\Http\Controllers;

use App\Tool;
use App\Command;
use App\Example;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\log;

class CommandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $tools = Tool::all();
        $commands = Command::latest()->paginate(10);
        return view('admin.command.index',compact('tools','commands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $lower = strtolower( $request->tool_slug);
        $slug = str_replace(" ","-",$lower);
        $lower = strtolower($request->command);
        $cmd_slug =str_replace(" ","-",$lower);
        $command = new Command;
        $command->tool_slug = $slug;
        $command->cmd_slug = $cmd_slug;
        $command->command = $request->command;
        $command->save();
  
        return response()->json(['code'=>200, 'message'=>'Command Created successfully','data' => $command], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Command  $command
     * @return \Illuminate\Http\Response
     */
    public function show(Command $command)
    {
        $command = Command::find($id);

        return response()->json($command);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Command  $command
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
    
        $command = Command::find ($request->id);
        $command->command = $request->command;
        Log::info($command);
        $command->save();
        return response()->json($command);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Command  $command
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Command $command)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Command  $command
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $example = Example::where('cmd_slug', $request->cmd_slug)->get();
        Log::info($example);
        if(count($example) > 0)
        {
            return response()->json([
                "message" => "Unable To Delete This Command"
            ]); 
        }
        else
        {
            $command = Command::where('cmd_slug', $request->cmd_slug)->delete();
            return response()->json([
                "message" => "Command Deleted Successfully"
            ]);
        }



        // $command = Command::find ($request->id)->delete();
        // return response()->json(['success'=>'Command Deleted successfully']);
    }
}
