<?php

namespace App\Http\Controllers;

use DB;
use App\Tool;
use App\Command;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class ToolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        $post = Tool::latest()->paginate(10);
        return view('admin.tool.index',compact('post'));
    }



    public function addPost(Request $request)
    {
        Log::info('YOU are in Tool Controller');
        $lower = strtolower( $request->title);
        Log::info($lower);
        $slug = str_replace(" ","-",$lower);
        Log::info($slug);
        $post = new Tool;
        $post->slug = $slug;
        $post->tool = $request->title;
        $post->about = $request->body;
        $post->save();
        return response()->json($post);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function editPost(request $request)
    {
        
        $lower = strtolower( $request->title);
        $slug = str_replace(" ","-",$lower);
        $post = Tool::find ($request->id);
        $post->slug = $slug;
        $post->tool = $request->title;
        $post->about = $request->body;
        $post->save();
        return response()->json($post);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function show(Tool $tool)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tool $tool)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function deletePost(request $request)
    {

        $command = Command::where('tool_slug', $request->slug)->get();
            if(count($command) > 0)
	        {
                return response()->json([
                    "message" => "Unable To Delete This Tool"
                ]); 
	        }
            else
            {
                $tool = Tool::where('slug', $request->slug)->delete();
                return response()->json([
                    "message" => "Tool Deleted"
                ]);
	        }
        
       
    }
}
