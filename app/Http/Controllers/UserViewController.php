<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tool;
use App\Command;
use App\Example;
use Illuminate\Support\Facades\Log;

class UserViewController extends Controller
{


    
    public function tool()
    {
        $commands = Command::latest()->paginate(10);
        $tools = Tool::latest()->get();
        return view('userview.tool',compact('tools','commands'));
    }


    public function command($slug)
    {
      
        $commands = Command::where('tool_slug', '=', $slug)->get();
        if(count($commands) > 0)
            return view('userview.command',compact('commands','slug'));
        else
            return view('userview.error');
    }

        

    public function example($tool_slug, $slug)
    {
        $examples = Example::where('cmd_slug', '=', $slug)->get();
        if(count($examples) > 0)
            return view('userview.example',compact('examples'));
        else
            return view('userview.error');
    }
}
