<?php

namespace App\Http\Controllers;

use App\Example;
use App\Tool;
use DB;
use App\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ExampleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tools = Tool::all();
        $commands =Command::all();
        $examples = Example::latest()->paginate(10);
        return view('admin.example.index',compact('tools','commands','examples'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addExample(Request $request)
    {
       
        $example = new Example;
        Log::info("This is example controller");
        $example->cmd_slug = $request->cmd_slug;
        Log::info($example);
        $example->example = $request->example;
        Log::info($example);
        $example->save();
        return response()->json($example);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function show(Example $example)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function editExample(Request $request)
    {
        Log::info('This is example edit controller');
        $example = Example::find ($request->id);
        Log::info('$example');
        Log::info($example);
        $example->example = $request->body;
        $example->save();
        return response()->json($example);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Example $example)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Example  $example
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $example = Example::find ($request->id)->delete();

        return response()->json(['success'=>'Command Deleted successfully']);
    }



    public function command_list($cmd_slug)
    {
        
        $commands = DB::table("commands")
        ->where("tool_slug",$cmd_slug)
        ->pluck("command","cmd_slug");
        return response()->json($commands);
    }
}
